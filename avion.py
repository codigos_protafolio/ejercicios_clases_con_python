class Avion():
    """**********************
     * Atributos
     ********************"""

    """********************
      * Constructores
      ********************"""
    def __init__(self, color, tamanio):
        self.color = color
        self.tamanio = tamanio


    """**********************
     * Metodos
     * (Acciones de la clase)
     ************************"""
    def aterrizar(self):
        print("Aterrizando.........")
        
    def despegar(self):
        print("Despegando.........")
        
    def frenar(self):
        print("Frenando.........")
        