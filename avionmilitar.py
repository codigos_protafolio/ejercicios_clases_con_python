from avion import Avion

class AvionMilitar(Avion):
    """*********************
     * Constructor
     *********************"""
    def __init__(self, color: str, tamanio: float, misiles: int):
        Avion.__init__(self, color, tamanio)
        self.misiles = misiles

    """modificadores"""

    def setMisiles(self, misiles: int):
        self.misiles = misiles

    """*******************
     * Métodos
     *******************"""
    def detectar_amenaza(self, amenaza: bool):
        if amenaza:
            self.disparar()
        else:
            print("No es una amenaza")
        

    def disparar(self):
        if self.misiles > 0:
            print("Disparando.......")
            self.misiles -= 1
        else:
            print("No hay munición")
            
        