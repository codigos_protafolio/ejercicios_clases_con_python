from avioncarga import AvionCarga
from avionpasajeros import AvionPasajeros
from avionmilitar import AvionMilitar

print("********avion carga******")
objAvionCarga = AvionCarga("Gris", 150.5)
objAvionCarga.cargar()
objAvionCarga.despegar()
objAvionCarga.aterrizar()
objAvionCarga.descargar()
#print(objAvionCarga.color)
print("********avion pasajeros******")
objAvionPasajeros = AvionPasajeros("Plateado", 150.5, 160)
objAvionPasajeros.despegar()
objAvionPasajeros.servir()
objAvionPasajeros.aterrizar()
print("********avion militar******")
objAvionMilitar = AvionMilitar("Azul",12.5,2)
objAvionMilitar.setMisiles(6)
objAvionMilitar.despegar()
objAvionMilitar.detectar_amenaza(False)
for i in range(0,8):
    objAvionMilitar.detectar_amenaza(True)

