from avion import Avion

class AvionCarga(Avion):
    """*********************
     * Constructor
     *********************"""
    def __init__(self, color: str, tamanio: float):
        super(AvionCarga, self).__init__(color, tamanio)
        #Avion.__init__(self, color, tamanio)

    """*******************
     * Métodos
     *******************"""
    def cargar(self):
        print("Cargando..........")

    def descargar(self):
        print("Descargando...........")