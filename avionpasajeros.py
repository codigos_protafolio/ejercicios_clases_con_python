from avion import Avion

class AvionPasajeros(Avion):
    """*********************
     * Constructor
     *********************"""
    def __init__(self, color: str, tamanio: float, pasajeros: int):
        Avion.__init__(self, color, tamanio)
        self.pasajeros = pasajeros

    """*****************
     * Modificador
     *****************"""
    def setPasajeros(self, pasajeros: int):
        self.pasajeros = pasajeros

    """*******************
     * Métodos
     *******************"""
    def servir(self):
        print("Sirviendo a los pasajeros.......")
